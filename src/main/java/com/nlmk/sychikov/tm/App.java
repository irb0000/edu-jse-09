package com.nlmk.sychikov.tm;

import com.nlmk.sychikov.tm.controller.ProjectController;
import com.nlmk.sychikov.tm.controller.SystemController;
import com.nlmk.sychikov.tm.controller.TaskController;
import com.nlmk.sychikov.tm.entity.Task;
import com.nlmk.sychikov.tm.repository.ProjectRepository;
import com.nlmk.sychikov.tm.repository.TaskRepository;
import com.nlmk.sychikov.tm.service.ProjectService;
import com.nlmk.sychikov.tm.service.TaskService;

import java.util.Scanner;

import static com.nlmk.sychikov.tm.constant.TerminalConst.*;

/**
 * Task manager
 *
 * @author Irb
 */
public class App {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    protected final ProjectService projectService = new ProjectService(projectRepository);

    protected final TaskService taskService = new TaskService(taskRepository);

    protected final ProjectController projectController = new ProjectController(projectService);

    protected final TaskController taskController = new TaskController(taskService);

    protected final SystemController systemController = new SystemController();

    protected final Scanner scanner = new Scanner(System.in);

    {
        projectRepository.create("Test project 1");
        projectRepository.create("Test project 2");
        final Task task = taskRepository.create("Task demo 1");
        task.setDescription("Description 1");
        taskRepository.create("Task demo 2");
    }

    /**
     * Main (start point)
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        final App app = new App();
        app.run(args);
        app.systemController.displayWelcome();
        app.process();
    }

    /**
     * Command line args processor
     *
     * @param args command line arguments
     */
    private void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    /**
     * One command processor
     *
     * @param param command
     * @return return value
     */
    private int run(final String param) {
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case VERSION:
                return systemController.displayVersion();
            case ABOUT:
                return systemController.displayAbout();
            case HELP:
                return systemController.displayHelp();
            case EXIT:
                return systemController.exit();

            case PROJECT_CLEAR:
                return projectController.clearProject();
            case PROJECT_CREATE:
                return projectController.createProject();
            case PROJECT_LIST:
                return projectController.listProject();
            case PROJECT_VIEW_BY_INDEX:
                return projectController.viewProjectByIndex();
            case PROJECT_VIEW_BY_ID:
                return projectController.viewProjectById();
            case PROJECT_REMOVE_BY_ID:
                return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX:
                return projectController.removeProjectByIndex();
            case PROJECT_REMOVE_BY_NAME:
                return projectController.removeProjectByName();
            case PROJECT_UPDATE_BY_INDEX:
                return projectController.updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID:
                return projectController.updateProjectById();

            case TASK_CLEAR:
                return taskController.clearTask();
            case TASK_CREATE:
                return taskController.createTask();
            case TASK_LIST:
                return taskController.listTask();
            case TASK_VIEW_BY_INDEX:
                return taskController.viewTaskByIndex();
            case TASK_VIEW_BY_ID:
                return taskController.viewTaskById();
            case TASK_REMOVE_BY_ID:
                return taskController.removeTaskById();
            case TASK_REMOVE_BY_INDEX:
                return taskController.removeTaskByIndex();
            case TASK_REMOVE_BY_NAME:
                return taskController.removeTaskByName();
            case TASK_UPDATE_BY_INDEX:
                return taskController.updateTaskByIndex();
            case TASK_UPDATE_BY_ID:
                return taskController.updateTaskById();

            default:
                return systemController.displayError();
        }
    }

    /**
     * Infinite loop command processor with console input.
     * The "exit" command stops the loop.
     */
    private void process() {
        String command = "";
        while (!EXIT.equals(command)) {
            System.out.print("command:> ");
            command = scanner.nextLine();
            run(command);
            System.out.println();
        }
    }

}
