package com.nlmk.sychikov.tm.controller;

import com.nlmk.sychikov.tm.entity.Task;
import com.nlmk.sychikov.tm.service.TaskService;

public class TaskController extends AbstractController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    /**
     * Add new task
     *
     * @return return value
     */
    public int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.print("Enter task name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter task description: ");
        final String description = scanner.nextLine();
        if (taskService.create(name, description) == null) {
            System.out.println("[FAILED]");
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Update task by index
     *
     * @return return value
     */
    public int updateTaskByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");
        final int index = indexInputProcessor("Enter task index: ");
        if (index == -1)
            return 0;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[Task index='" + index + "' is not found!]");
            return 0;
        }
        System.out.print("Enter task name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter task description: ");
        final String description = scanner.nextLine();
        taskService.update(task.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Update task by id
     *
     * @return return value
     */
    public int updateTaskById() {
        System.out.println("[UPDATE TASK BY ID]");
        final Long id = idInputProcessor("Enter task id: ");
        if (id == -1)
            return 0;
        final Task task = taskService.findById(id);
        if (task == null) {
            System.out.println("[Task id='" + id.toString() + "' is not found!]");
            return 0;
        }
        System.out.print("Enter task name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter task description: ");
        final String description = scanner.nextLine();
        taskService.update(id, name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes task by name
     *
     * @return return value
     */
    public int removeTaskByName() {
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.print("Enter task name: ");
        final String name = scanner.nextLine();
        final Task task = taskService.removeByName(name);
        if (task == null) System.out.println("[Task '" + name + "' is not found!]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes task by id
     *
     * @return return value
     */
    public int removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        final Long id = idInputProcessor("Enter task id: ");
        if (id == -1)
            return 0;
        final Task task = taskService.removeById(id);
        if (task == null)
            System.out.println("[Task with id=" + id.toString() + " is not found!]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes task by index
     *
     * @return return value
     */
    public int removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        final int index = indexInputProcessor("Enter task index: ");
        if (index == -1)
            return 0;
        final Task task = taskService.removeByIndex(index);
        if (task == null)
            System.out.println("[Task with index=" + (index + 1) + " is not found!]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Clear all tasks
     *
     * @return return value
     */
    public int clearTask() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Show the task
     *
     * @param task what task should be printed
     */
    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASKS]");
        System.out.println("ID:" + task.getId());
        System.out.println("NAME:" + task.getName());
        System.out.println("DESCRIPTION:" + task.getDescription());
        System.out.println("[OK]");
    }

    /**
     * Shows task by index
     *
     * @return return value
     */
    public int viewTaskByIndex() {
        final int index = indexInputProcessor("Enter task index: ");
        if (index == -1)
            return 0;
        final Task task = taskService.findByIndex(index);
        if (task == null)
            System.out.println("Task with index=" + (index + 1) + " is not found!");
        else viewTask(task);
        return 0;
    }

    /**
     * Shows task by id
     *
     * @return return value
     */
    public int viewTaskById() {
        final Long id = idInputProcessor("Enter task id: ");
        if (id == -1)
            return 0;
        final Task task = taskService.findById(id);
        if (task == null)
            System.out.println("Task with index=" + id.toString() + " is not found!");
        else viewTask(task);
        return 0;
    }

    /**
     * List all tasks
     *
     * @return return value
     */
    public int listTask() {
        System.out.println("[LIST TASKS]");
        int index = 1;
        for (Task task : taskService.findAll()) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName() + ": " + task.getDescription());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

}
